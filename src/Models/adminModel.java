/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;

/**
 *
 * @author Alex
 */
public class adminModel implements Serializable{
    private String categoria;
    private String pelicula;
    private String formato;
    private String duracion;
    private String precio_boleto2D;
    private String precio_boleto3D;

    public adminModel(String categoria, String pelicula, String formato, String duracion, String precio_boleto2D, String precio_boleto3D) {
        this.categoria = categoria;
        this.pelicula = pelicula;
        this.formato = formato;
        this.duracion = duracion;
        this.precio_boleto2D = precio_boleto2D;
        this.precio_boleto3D = precio_boleto3D;
    }

    public adminModel() {
       
    }
    
    public void adminModel(){
        this.categoria = "";
        this.pelicula = "";
        this.formato = "";
        this.duracion = "";
        this.precio_boleto2D = "";
        this.precio_boleto3D = "";
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPelicula() {
        return pelicula;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getPrecio_boleto2D() {
        return precio_boleto2D;
    }

    public void setPrecio_boleto2D(String precio_boleto2D) {
        this.precio_boleto2D = precio_boleto2D;
    }

    public String getPrecio_boleto3D() {
        return precio_boleto3D;
    }

    public void setPrecio_boleto3D(String precio_boleto3D) {
        this.precio_boleto3D = precio_boleto3D;
    }
    
}
