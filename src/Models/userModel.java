/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;

/**
 *
 * @author Alex
 */
public class userModel implements Serializable{
    private String ID_boleto;
    private String num_boleto;
    private String tipo_boleto;
    private String cant_niños;
    private String cant_adultos;
    private String cliente;
    private String tipo_pago;
    private String monto;
    private String cant_pagar;
    private String cambio;
    private String num_asientos;
    private String categorias;
    private String pelicula;
    private String formato;
    private String Duracion;

    public userModel(String ID_boleto, String num_boleto, String tipo_boleto, String cant_niños, String cant_adultos, String cliente, String tipo_pago, String monto, String cant_pagar, String cambio, String num_asientos, String categorias, String pelicula, String formato, String Duracion) {
        this.ID_boleto = ID_boleto;
        this.num_boleto = num_boleto;
        this.tipo_boleto = tipo_boleto;
        this.cant_niños = cant_niños;
        this.cant_adultos = cant_adultos;
        this.cliente = cliente;
        this.tipo_pago = tipo_pago;
        this.monto = monto;
        this.cant_pagar = cant_pagar;
        this.cambio = cambio;
        this.num_asientos = num_asientos;
        this.categorias = categorias;
        this.pelicula = pelicula;
        this.formato = formato;
        this.Duracion = Duracion;
    }
    

    

    public userModel() {
        
    }
    
    public void usermodel (){
        this.ID_boleto = "";
        this.num_boleto = "";
        this.tipo_boleto = "";
        this.cant_niños = "";
        this.cant_adultos = "";
        this.cliente = "";
        this.tipo_pago = "";
        this.monto = "" ;
        this.cant_pagar = "";
        this.cambio = "";
        this.num_asientos = "";
        this.categorias = "";
        this.pelicula = "";
        this.formato = "";
        this.Duracion = "";
    }

    public String getID_boleto() {
        return ID_boleto;
    }

    public void setID_boleto(String ID_boleto) {
        this.ID_boleto = ID_boleto;
    }

    public String getNum_boleto() {
        return num_boleto;
    }

    public void setNum_boleto(String num_boleto) {
        this.num_boleto = num_boleto;
    }

    public String getTipo_boleto() {
        return tipo_boleto;
    }

    public void setTipo_boleto(String tipo_boleto) {
        this.tipo_boleto = tipo_boleto;
    }

    public String getCant_niños() {
        return cant_niños;
    }

    public void setCant_niños(String cant_niños) {
        this.cant_niños = cant_niños;
    }

    public String getCant_adultos() {
        return cant_adultos;
    }

    public void setCant_adultos(String cant_adultos) {
        this.cant_adultos = cant_adultos;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getCant_pagar() {
        return cant_pagar;
    }

    public void setCant_pagar(String cant_pagar) {
        this.cant_pagar = cant_pagar;
    }

    public String getCambio() {
        return cambio;
    }

    public void setCambio(String cambio) {
        this.cambio = cambio;
    }

    public String getNum_asientos() {
        return num_asientos;
    }

    public void setNum_asientos(String num_asientos) {
        this.num_asientos = num_asientos;
    }

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }

    public String getPelicula() {
        return pelicula;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getDuracion() {
        return Duracion;
    }

    public void setDuracion(String Duracion) {
        this.Duracion = Duracion;
    }

    
    
}
