/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;


import Models.adminModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.JFileChooser;

import paneles.panelpeliculas;


/**
 *
 * @author Alex
 */
public class adminController implements ActionListener {

    panelpeliculas cinemaxadmin;
    JFileChooser j;
    adminModel admin;

    public adminController(panelpeliculas cinemaxadmin) {
        super();
        this.cinemaxadmin = cinemaxadmin;
        j = new JFileChooser();
        admin = new adminModel();
    }

    public adminModel getAdmin() {
        return admin;
    }

    public void setAdmin(adminModel admin) {
        this.admin = admin;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "save":

                writeadmin();

                break;

            case "clear":
                cinemaxadmin.clear();
                break;
        }

    }

    private void writeadmin() {
        File file = new File("Peliculas.txt");
        FileWriter fichero = null;
        PrintWriter pw;
        if (!file.exists()) {
            try {
                fichero = new FileWriter(file, true);
                pw = new PrintWriter(fichero);

                admin = cinemaxadmin.obtenerinfo();
                String category = admin.getCategoria();
                String peli = admin.getPelicula();
                String formato = admin.getFormato();
                String duracion = admin.getDuracion();
                String precio_boleto2D = admin.getPrecio_boleto2D();
                String precio_boleto3D = admin.getPrecio_boleto3D();

                pw.println(category);
                pw.println(peli);
                pw.println(formato);
                pw.println(duracion);
                pw.println(precio_boleto2D);
                pw.println(precio_boleto3D);
                pw.append("\r\n");

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // Nuevamente aprovechamos el finally para 
                    // asegurarnos que se cierra el fichero.
                    if (null != file) {
                        fichero.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } else {
            try {
                fichero = new FileWriter(file, true);
                pw = new PrintWriter(fichero);

                admin = cinemaxadmin.obtenerinfo();
                String category = admin.getCategoria();
                String peli = admin.getPelicula();
                String formato = admin.getFormato();
                String duracion = admin.getDuracion();
                String precio_boleto2D = admin.getPrecio_boleto2D();
                

                pw.println(category+" , "+peli+" , "+formato+" , "+duracion+" , "+precio_boleto2D);
                pw.append("\r\n");

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // Nuevamente aprovechamos el finally para 
                    // asegurarnos que se cierra el fichero.
                    if (null != file) {
                        fichero.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

}
