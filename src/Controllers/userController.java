/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;


import Models.userModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.JFileChooser;
import paneles.panelboletos;
import views.userPrincipal;

/**
 *
 * @author Alex
 */
public class userController implements ActionListener {

    panelboletos cinemax;
    JFileChooser j;
    userModel model;

    public userController(panelboletos c) {
        super();
        cinemax = c;
        j = new JFileChooser();
        model = new userModel();
    }

    public userModel getModel() {
        return model;
    }

    public void setModel(userModel model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "save":
                writeuser();
                break;
            case "clear":
                cinemax.clear();
                break;
        }

    }

    private void writeuser() {
        File file = new File("Registro_ventas.txt");
        FileWriter fichero = null;
        PrintWriter pw;
        if (!file.exists()) {
            try {
                fichero = new FileWriter(file, true);
                pw = new PrintWriter(fichero);

                model = cinemax.obtenerinfo();
                String ID_boleto = model.getID_boleto();
                String num_boleto = model.getNum_boleto();
                String tipo_boleto = model.getTipo_boleto();
                String cant_niños = model.getCant_niños();
                String cant_adultos = model.getCant_adultos();
                String cliente = model.getCliente();
                String tipo_pago = model.getTipo_pago();
                String monto = model.getMonto();
                String cant_pagar = model.getCant_pagar();
                String cambio = model.getCambio();
                String num_asientos = model.getNum_asientos();
                String categorias = model.getCategorias();
                String pelicula = model.getPelicula();
                String formato = model.getFormato();
                String Duracion = model.getDuracion();

                pw.println(ID_boleto);
                pw.println(num_boleto);
                pw.println(formato);
                pw.println(tipo_boleto);
                pw.println(cant_adultos);
                pw.println(cant_niños);
                pw.println(cliente);
                pw.println(tipo_pago);
                pw.println(monto);
                pw.println(cant_pagar);
                pw.println(cambio);
                pw.println(num_asientos);
                pw.println(categorias);
                pw.println(pelicula);
                pw.println(Duracion);
                pw.append("\r\n");

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // Nuevamente aprovechamos el finally para 
                    // asegurarnos que se cierra el fichero.
                    if (null != file) {
                        fichero.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } else {
            try {
                fichero = new FileWriter(file, true);
                pw = new PrintWriter(fichero);

                model = cinemax.obtenerinfo();
                String ID_boleto = model.getID_boleto();
                String num_boleto = model.getNum_boleto();
                String tipo_boleto = model.getTipo_boleto();
                String cant_niños = model.getCant_niños();
                String cant_adultos = model.getCant_adultos();
                String cliente = model.getCliente();
                String tipo_pago = model.getTipo_pago();
                String monto = model.getMonto();
                String cant_pagar = model.getCant_pagar();
                String cambio = model.getCambio();
                String num_asientos = model.getNum_asientos();
                String categorias = model.getCategorias();
                String pelicula = model.getPelicula();
                String formato = model.getFormato();
                String Duracion = model.getDuracion();

               pw.println(ID_boleto+" , "+num_boleto+" , "+formato+" , "+tipo_boleto+" , "+cant_adultos+" , "+cant_niños+" , "+cliente+" , "+tipo_pago+" , "+monto+" , "+cant_pagar+" , "+cambio+" , "+num_asientos+" , "+categorias+" , "+pelicula+" , "+Duracion);
                pw.append("\r\n");

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // Nuevamente aprovechamos el finally para 
                    // asegurarnos que se cierra el fichero.
                    if (null != file) {
                        fichero.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

}
